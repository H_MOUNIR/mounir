
function cambioIdioma(){


    // Configurar i18n
    $.i18n().load({
        'en': 'en.json',
        'es': 'es.json'
    }).done(function() {
        // Inicializar en ESPAÑOL por defecto
        $('html').i18n();

        // Cambiar idioma al hacer clic en los botones
        $('#lang-en').on('click', function() {
            $.i18n().locale = 'en';
            $('html').i18n();
        });

        $('#lang-es').on('click', function() {
            $.i18n().locale = 'es';
            $('html').i18n();
        });
    });
}

