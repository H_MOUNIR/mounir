//$(document).ready(function() {

function traducir() {
    // Inicializa i18next con el backend para cargar archivos JSON
    i18next.use(i18nextHttpBackend).init({
        lng: 'es', // Idioma inicial
        debug: true,
        backend: {
            loadPath: 'jsonTrad/{{lng}}.json' // Ruta para los archivos de traducción
        }
    }, function(err, t) {
        // Inicializa el plugin de jQuery para i18next
        jqueryI18next.init(i18next, $);
        
        // Traduce el contenido inicial
        $('body').localize();
    });

    // Evento para cambiar el idioma
    $('#change-language').on('click', function() {
        const newLang = i18next.language === 'es' ? 'en' : 'es';
        i18next.changeLanguage(newLang, function() {
            $('body').localize();
        });
    });
};
