const d = document;
const w = window;

// Declaro un limite de cuando se va a mostrar el boton
const showButtonLimit = 1000;
// De una vez identifico que classe quiero capturar con el querySelector
const btn_scroll = d.querySelector(".scroll-top-btn");


const scrollContainer = () => {
  return document.documentElement || document.body;
};

//De esta forma no tienes que exportar la función, siempre se invocara cuando
//se haga escroll

d.addEventListener("scroll", () => {
  if (scrollContainer().scrollTop > showButtonLimit) {
    btn_scroll.classList.remove("hidden");
  } else {
    btn_scroll.classList.add("hidden");
  }
});

//La función que llamara el boton para volver al incio.
const goTop = () => {
  d.body.scrollIntoView({
    behavior: "smooth",
  });
};

//Capturamos el evento click del boton
btn_scroll.addEventListener("click", goTop);